package persistence;

import com.alejandrovp.logicomp.LogiCompApplication;
import com.alejandrovp.logicomp.entity.CityEntity;
import com.alejandrovp.logicomp.entity.TruckEntity;
import com.alejandrovp.logicomp.util.TruckState;
import com.alejandrovp.logicomp.persistence.CityRepository;
import com.alejandrovp.logicomp.persistence.TruckRepository;
import jakarta.transaction.Transactional;
import lombok.extern.java.Log;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = LogiCompApplication.class)
@Log
public class TruckEntityTests {
    @Autowired
    private TruckRepository truckRepository;

    @Autowired
    private CityRepository cityRepository;

    private TruckEntity testTruckEntity;

    @BeforeEach
    public void setup() {
        Optional<CityEntity> optFound = cityRepository.findByName("TEST-CITY");
        CityEntity testCityEntity;
        if(optFound.isEmpty()) {
            CityEntity c = new CityEntity();
            c.setName("TEST-CITY");
            testCityEntity = cityRepository.save(c);
        }
        else {
            testCityEntity = optFound.get();
        }

        testTruckEntity = new TruckEntity();
        testTruckEntity.setMaxCapacityTons(5);
        testTruckEntity.setState(TruckState.OK);
        testTruckEntity.setCurrentLocation(testCityEntity);
        testTruckEntity.setDrivers(null);
        testTruckEntity.setCurrentOrder(null);
    }

    @Test
    @DisplayName("Check that test components are injected")
    public void injectedComponentsAreNotNull() {
        assertThat(truckRepository).isNotNull();
        assertThat(cityRepository).isNotNull();
    }

    @Test
    @DisplayName("Insert a test TruckEntity into the database")
    public void insertTruck() {
        truckRepository.save(testTruckEntity);
    }

    @Test
    @DisplayName("Query and show information about a truck")
    @Transactional
    public void findTruckById() {
        truckRepository.save(testTruckEntity);

        Optional<TruckEntity> OptFound = truckRepository.findById((int) testTruckEntity.getId());
        assertThat(OptFound).isPresent();
        TruckEntity found = OptFound.get();
        assertThat(found.getId()).isEqualTo(testTruckEntity.getId());
    }
}
