package com.alejandrovp.logicomp.entity;

import com.alejandrovp.logicomp.util.CargoStatus;
import jakarta.persistence.*;
import lombok.Data;

import static jakarta.persistence.EnumType.STRING;

@Entity
@Data
public class CargoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;
    private String description;

    private int weightKg;

    @Enumerated(STRING)
    private CargoStatus status;
}

