package com.alejandrovp.logicomp.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Set;

@Entity
@Data
@Table(name = "order_table")
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private boolean complete;

    @OneToMany(mappedBy = "currentOrder")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<TruckEntity> assignedTrucks;

    // TODO: we'll need some logic to pair driver-truck and distance (hours)
    // TODO: no driver can exceed the 176 hour limit
    @OneToMany(mappedBy = "currentOrder")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<DriverEntity> assignedDrivers;

    @OneToMany
    private WaypointEntity[] route;
}
