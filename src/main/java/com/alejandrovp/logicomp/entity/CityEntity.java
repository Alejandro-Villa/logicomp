package com.alejandrovp.logicomp.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Set;

@Entity
@Data
@Table(
        uniqueConstraints = @UniqueConstraint(columnNames = {"name"})
)
public class CityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    // CityEntity Name
    private String name;

    // Trucks currently in the city
    @OneToMany(mappedBy = "currentLocation")
    private Set<TruckEntity> trucks;

    @OneToMany(mappedBy = "currentLocation")
    private Set<DriverEntity> drivers;
}
