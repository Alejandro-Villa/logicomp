package com.alejandrovp.logicomp.entity;

import com.alejandrovp.logicomp.util.PlateNumber;
import com.alejandrovp.logicomp.util.TruckState;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

import static jakarta.persistence.EnumType.STRING;

@Entity
@Data
public class TruckEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Embedded
    @Column(name = "plate_number", unique = true)
    private PlateNumber plateNumber;

    // Measured in Tons
    private int maxCapacityTons;

    @Enumerated(STRING)
    private TruckState state;

    @ManyToOne
    @JoinColumn(name = "current_city_id", nullable = false)
    private CityEntity currentLocation;

    @OneToMany(mappedBy = "assignedTruck")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<DriverEntity> drivers;

    @ManyToOne
    @JoinColumn(name = "current_order_id")
    private OrderEntity currentOrder;
}