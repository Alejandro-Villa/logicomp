package com.alejandrovp.logicomp.entity;

import com.alejandrovp.logicomp.util.WaypointType;
import jakarta.persistence.*;
import lombok.Data;

import static jakarta.persistence.EnumType.STRING;

@Entity
@Data
public class WaypointEntity {
    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private CityEntity location;

    @ManyToOne
    @JoinColumn(name = "cargo_id")
    private CargoEntity cargo;

    @Enumerated(STRING)
    private WaypointType type;
}

