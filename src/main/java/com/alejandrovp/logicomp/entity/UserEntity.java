package com.alejandrovp.logicomp.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Collection;

@Entity
@Table(name = "user_account",
        uniqueConstraints = @UniqueConstraint(columnNames = {"username"}))
@Data
public class UserEntity {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private String username;

    @Column
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_accounts_to_roles",
            joinColumns = @JoinColumn(name = "user_account_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"),
            uniqueConstraints = {@UniqueConstraint(columnNames = {"user_account_id", "role_id"})}
    )
    private Collection<UserRoleEntity> roles;

    @OneToOne
    private DriverEntity driver;

    public boolean isDriver() {
        return (driver != null);
    }
}
