package com.alejandrovp.logicomp.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class CountryMap {
    // TODO: implement how to efficiently store city graph and distances
    @Id
    @GeneratedValue
    private long id;
}
