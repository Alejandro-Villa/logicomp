package com.alejandrovp.logicomp.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "security_roles",
        uniqueConstraints = @UniqueConstraint(columnNames = {"textcode"}))
public class UserRoleEntity {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private String textcode;
}
