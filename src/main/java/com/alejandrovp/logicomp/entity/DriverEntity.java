package com.alejandrovp.logicomp.entity;

import com.alejandrovp.logicomp.util.DriverStatus;
import jakarta.persistence.*;
import lombok.Data;

import static jakarta.persistence.EnumType.STRING;

@Entity
@Data
public class DriverEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String surname;

    // Working hours MUST be less than maximum
    // Counter gets reset each month
    @Column(columnDefinition = "integer default 0")
    private int workingHoursCounter;
    private static final int maxWorkingHours = 176;

    @Enumerated(STRING)
    private DriverStatus status;

    @ManyToOne
    @JoinColumn(name = "current_city_id", nullable = false)
    private CityEntity currentLocation;

    @ManyToOne
    @JoinColumn(name = "assigned_truck_id")
    private TruckEntity assignedTruck;

    @ManyToOne
    @JoinColumn(name = "current_order_id")
    private OrderEntity currentOrder;
}

