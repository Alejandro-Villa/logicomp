package com.alejandrovp.logicomp.security;

import org.springframework.stereotype.Service;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.HashMap;
import java.util.Map;

@Service
public class SecurityTokenManager {
    private final Map<String, String> usernames = new HashMap<>();

    public String createToken(String username) {
        String token = RandomStringUtils.randomAlphanumeric(32);
        usernames.put(token, username);
        return token;
    }

    public String getUsernameByToken(String token) { return usernames.get(token); }
}
