package com.alejandrovp.logicomp.security;

import com.alejandrovp.logicomp.entity.UserEntity;
import com.alejandrovp.logicomp.entity.UserRoleEntity;
import com.alejandrovp.logicomp.persistence.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.stream.Collectors;

@RequiredArgsConstructor
public class SecurityUserService implements UserDetailsService {
    private final UserRepository userRepository;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(username).orElseThrow();
        CustomUserDetails customUserDetails = new CustomUserDetails();
        customUserDetails.setId(userEntity.getId());
        customUserDetails.setUsername(userEntity.getUsername());
        customUserDetails.setPassword(userEntity.getPassword());
        customUserDetails.setRoles(userEntity.getRoles().stream().map(UserRoleEntity::getTextcode).collect(Collectors.toSet()));
        return customUserDetails;
    }
}
