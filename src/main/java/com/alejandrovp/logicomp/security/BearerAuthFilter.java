package com.alejandrovp.logicomp.security;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor
@Log
public class BearerAuthFilter extends OncePerRequestFilter {
    private final SecurityTokenManager securityTokenManager;
    private final UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain) throws ServletException, IOException {
        try {
            // Get token stored in request cookies
            //log.info("Cookies: " + Arrays.toString(request.getCookies()));
            String token;
            if(request.getCookies() != null) {
                Optional<Cookie> authCookie = Arrays.stream(request.getCookies())
                        .filter(cookie -> Objects.equals(cookie.getName(), "token"))
                        .findFirst();
                String authCookieValue = authCookie.map(Cookie::getValue).orElse(null);
                token = getBearerToken(request, authCookieValue);
                //log.info("Retrieved token " + token);
            }
            else {
                token = getBearerToken(request, "");
            }
            if(token != null) {
                String username = securityTokenManager.getUsernameByToken(token);
                UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails,
                        null,
                        userDetails.getAuthorities()
                );
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        } catch (Exception e) {
            logger.error("Could not authenticate user", e);
        }
        filterChain.doFilter(request, response);
    }

    private String getBearerToken(HttpServletRequest request, String authCookieValue) {
        // Check if token is in headers
        // This is for API mainly
        String authHeader = request.getHeader("Authorization");
        if(authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.substring(7);
        }

        return authCookieValue;
    }
}
