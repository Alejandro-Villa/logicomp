package com.alejandrovp.logicomp.persistence;

import com.alejandrovp.logicomp.entity.DriverEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DriverRepository extends CrudRepository<DriverEntity, Integer> {

    List<DriverEntity> findByName(String name);
    List<DriverEntity> findBySurname(String surname);
}
