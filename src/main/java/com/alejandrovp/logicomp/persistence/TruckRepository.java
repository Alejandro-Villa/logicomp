package com.alejandrovp.logicomp.persistence;

import com.alejandrovp.logicomp.entity.TruckEntity;
import com.alejandrovp.logicomp.util.PlateNumber;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TruckRepository extends CrudRepository<TruckEntity, Integer> {

    Optional<TruckEntity> findByPlateNumber(PlateNumber plateNumber);
}
