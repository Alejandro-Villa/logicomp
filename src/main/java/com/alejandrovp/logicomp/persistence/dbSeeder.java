package com.alejandrovp.logicomp.persistence;

import com.alejandrovp.logicomp.dto.*;
import com.alejandrovp.logicomp.entity.CityEntity;
import com.alejandrovp.logicomp.entity.UserEntity;
import com.alejandrovp.logicomp.entity.UserRoleEntity;
import com.alejandrovp.logicomp.entity.WaypointEntity;
import com.alejandrovp.logicomp.mapper.CargoEntityDtoMapper;
import com.alejandrovp.logicomp.mapper.CityEntityDtoMapper;
import com.alejandrovp.logicomp.mapper.DriverEntityDtoMapper;
import com.alejandrovp.logicomp.mapper.WaypointEntityDtoMapper;
import com.alejandrovp.logicomp.service.*;
import com.alejandrovp.logicomp.util.*;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@RequiredArgsConstructor
@Log
@Component
@Transactional
public class dbSeeder {
    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthService authService;

    private final OrderService orderService;
    private final CargoService cargoService;
    private final CityService cityService;
    private final DriverService driverService;
    private final DriverEntityDtoMapper driverEntityDtoMapper;
    private final TruckService truckService;

    private final WaypointRepository waypointRepository;

    private final CityEntityDtoMapper cityEntityDtoMapper;
    private final CargoEntityDtoMapper cargoEntityDtoMapper;
    private final WaypointEntityDtoMapper waypointEntityDtoMapper;

    @EventListener({ContextStartedEvent.class, ContextRefreshedEvent.class})
    public void seed() {
        log.info("Calling dbSeeder#seed");
        seedUserRoleTable();
        seedUserTable();

        seedExampleData();
    }

    private void seedExampleData() {
        seedCityTable();
        seedCargoTable();
        seedDriverTable();
        seedTruckTable();
        seedWaypointTable();
        seedOrderTable();
    }

    private void seedWaypointTable() {
        log.info("Seeding Waypoints table");

        CityEntity granada = cityEntityDtoMapper.DtoToEntity(cityService.getCityByName("Granada").orElseThrow());
        CityEntity seville = cityEntityDtoMapper.DtoToEntity(cityService.getCityByName("Seville").orElseThrow());

        List<WaypointEntity> allWaypoints = new ArrayList<>();
        waypointRepository.findAll().forEach(allWaypoints::add);

        boolean notSeeded = allWaypoints.stream().noneMatch(w ->
                w.getLocation().equals(granada) || w.getLocation().equals(seville));

        if(notSeeded) {
            WaypointEntity first = new WaypointEntity();
            first.setLocation(granada);
            first.setCargo(cargoEntityDtoMapper.DtoToEntity(cargoService.getCargoByName("Food").get(0)));
            first.setType(WaypointType.LOAD);

            WaypointEntity second = new WaypointEntity();
            second.setLocation(seville);
            second.setCargo(cargoEntityDtoMapper.DtoToEntity(cargoService.getCargoByName("Food").get(0)));
            second.setType(WaypointType.UNLOAD);

            waypointRepository.save(first);
            log.info("Saved waypoint " + first);

            waypointRepository.save(second);
            log.info("Saved waypoint " + second);

            log.info("Waypoint table seeded");
        }
        else
            log.info("Waypoint table already seeded, skipping...");
    }

    private void seedOrderTable() {
        log.info("Seeding Order table");
        if(orderService.getAll().isEmpty()) {
            OrderDto orderDto = new OrderDto();
            HashSet<TruckDto> trucks = new HashSet<>();
            trucks.add(truckService.getTruckByPlateNumber(new PlateNumber("5555AAA")).orElseThrow());
            orderDto.setAssignedTrucks(trucks);
            HashSet<DriverDto> drivers = new HashSet<>();
            drivers.add(driverService.getDriverByName("Bob").get(0));
            orderDto.setAssignedDrivers(drivers);
            orderDto.setComplete(false);

            ArrayList<WaypointDto> route = new ArrayList<>();
            waypointRepository.findAll().forEach(
                    waypointEntity -> route.add(waypointEntityDtoMapper.EntityToDto(waypointEntity))
            );
            WaypointDto[] array = route.toArray(new WaypointDto[0]);
            orderDto.setRoute(array);


            log.info("Saving " + orderDto);
            orderService.createOrder(orderDto);
        }
        else
            log.info("Order table already populated, skipping...");

        log.info("Order table seeded");
    }

    private void seedTruckTable() {
        log.info("Seeding Truck table");
        final TruckDto truck = new TruckDto();
        final PlateNumber plateNumber = new PlateNumber("5555AAA");
        if(truckService.getTruckByPlateNumber(plateNumber).isEmpty()) {
            log.info("Creating truck " + plateNumber);
            truck.setPlateNumber(plateNumber);
            truck.setState(TruckState.OK);
            truck.setMaxCapacityTons(5);
            truck.setCurrentLocation(
                    cityService.getCityByName("Granada").orElseThrow()
            );
            TruckDto created = truckService.createTuck(truck);
            DriverDto driverDto = driverService.getDriverByName("Bob").get(0);
            driverService.assignDriverToTruck(driverDto, created);

            log.info("Truck table seeded");
        }
        else
            log.info("Truck " + plateNumber + " already exists, skipping");

    }

    private void seedDriverTable() {
        log.info("Seeding Driver table");
        final String NAME = "Bob";
        if(driverService.getDriverByName(NAME).isEmpty()) {
            log.info("Creating driver '" + NAME + "'");
            DriverDto driver = new DriverDto();
            driver.setName(NAME);
            driver.setSurname(NAME);
            driver.setStatus(DriverStatus.RESTING);
            driver.setCurrentLocation(cityService.getCityByName("Granada").orElseThrow()); // Should never throw


            DriverDto created = driverService.createDriver(driver);

            log.info("Creating user for driver " + NAME);
            UserDto user = new UserDto();
            user.setUsername(NAME);
            user.setPassword(NAME);

            if(authService.signup(user, created.getId()) == null) { // User exists, so update it manually
                UserEntity found = userRepository.findByUsername(NAME).orElseThrow();
                found.setDriver(driverEntityDtoMapper.DtoToEntity(created));
                userRepository.save(found);
                authService.addRoleToUser(found.getUsername(), "DRIVER");
            }
        }
        else
            log.info("Driver already exists, skipping...");
        log.info("Driver table seeded");
    }

    private void seedCargoTable() {
        log.info("Seeding Cargo table");
        final String[] INITIAL_CARGO_NAMES = { "Steel", "Wood", "Food" };
        CargoDto cargo = new CargoDto();
        for(String name : INITIAL_CARGO_NAMES) {
            if(cargoService.getCargoByName(name).isEmpty()) {
                log.info("Creating cargo '" + name + "'");
                cargo.setName(name);
                cargo.setDescription("This is an example description, human-readable, of the " + name + " cargo");
                cargo.setWeightKg(100);
                cargo.setStatus(CargoStatus.READY);
                cargoService.createCargo(cargo);
            }
            else
                log.info("Cargo '" + name + "' already exists, skipping...");
        }

        log.info("Cargo table seeded");
    }

    private void seedCityTable() {
        log.info("Seeding City table");
        final String[] INITIAL_CITIES = { "Granada", "Seville", "Madrid" };
        CityDto city = new CityDto();
        for(String city_name : INITIAL_CITIES) {
            if(cityService.getCityByName(city_name).isEmpty()) {
                log.info("Creating City '" + city_name + "'");
                city.setName(city_name);
                cityService.createCity(city);
            } else
                log.info("City '" + city_name + "' already exists, skipping...");
        }
        log.info("City Table seeded");
    }

    private void seedUserRoleTable() {
        final String[] ROLES = {"ROLE_ADMIN", "ROLE_MANAGER", "ROLE_DRIVER", "ROLE_USER"};
        log.info("Seeding User Security Roles: " + Arrays.toString(ROLES));
        for (String role : ROLES) {
           if(userRoleRepository.findByTextcode(role).isEmpty()) {
               log.info("Saving role " + role);
               UserRoleEntity userRole = new UserRoleEntity();
               userRole.setTextcode(role);
               userRoleRepository.save(userRole);
           }
           else
               log.info(role + " already exists, skipping...");
        }
        log.info("User Role table seeded");
    }

    private void seedUserTable() {
        final UserEntity ADMIN = new UserEntity();
        final ArrayList<UserRoleEntity> DEFAULT_ROLES = new ArrayList<>();
        DEFAULT_ROLES.add(userRoleRepository.findByTextcode("ROLE_USER").orElseThrow());
        ADMIN.setUsername("admin");
        ADMIN.setPassword(passwordEncoder.encode("admin"));
        ArrayList<UserRoleEntity> adminRoles = new ArrayList<>(DEFAULT_ROLES);
        adminRoles.add(userRoleRepository.findByTextcode("ROLE_ADMIN").orElseThrow());
        ADMIN.setRoles(adminRoles);

        final UserEntity MANAGER = new UserEntity();
        MANAGER.setUsername("Alice");
        MANAGER.setPassword(passwordEncoder.encode("Alice"));
        ArrayList<UserRoleEntity> managerRoles = new ArrayList<>(DEFAULT_ROLES);
        managerRoles.add(userRoleRepository.findByTextcode("ROLE_MANAGER").orElseThrow());
        MANAGER.setRoles(managerRoles);

        final UserEntity[] INITIAL_USERS = { ADMIN, MANAGER };

        log.info("Seeding Users");

        for(UserEntity user : INITIAL_USERS) {
            if (userRepository.findByUsername(user.getUsername()).isEmpty()) {
                log.info("Saving user " + user.getUsername());
                userRepository.save(user);
            }
            else
                log.info("User " + user.getUsername() + " already exists, skipping...");
        }

        // Sanity check
        if( ! userRepository.findByUsername(ADMIN.getUsername()).orElseThrow().
                getRoles().contains(userRoleRepository.findByTextcode("ROLE_ADMIN").orElseThrow()))
            authService.addRoleToUser(ADMIN.getUsername(), "ADMIN");
        log.info("User table seeded");
    }
}
