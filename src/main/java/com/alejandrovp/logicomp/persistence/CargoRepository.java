package com.alejandrovp.logicomp.persistence;

import com.alejandrovp.logicomp.entity.CargoEntity;
import com.alejandrovp.logicomp.util.CargoStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CargoRepository extends CrudRepository<CargoEntity, Integer> {
    List<CargoEntity> findByName(String name);
    List<CargoEntity> findByStatus(CargoStatus status);
}
