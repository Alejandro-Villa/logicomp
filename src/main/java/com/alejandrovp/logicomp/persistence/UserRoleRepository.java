package com.alejandrovp.logicomp.persistence;

import com.alejandrovp.logicomp.entity.UserRoleEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRoleRepository extends CrudRepository<UserRoleEntity, Integer> {
    Optional<UserRoleEntity> findByTextcode(String textcode);
}
