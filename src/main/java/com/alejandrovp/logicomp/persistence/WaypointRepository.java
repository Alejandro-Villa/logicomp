package com.alejandrovp.logicomp.persistence;

import com.alejandrovp.logicomp.entity.WaypointEntity;
import org.springframework.data.repository.CrudRepository;

public interface WaypointRepository extends CrudRepository<WaypointEntity, Integer> {

}
