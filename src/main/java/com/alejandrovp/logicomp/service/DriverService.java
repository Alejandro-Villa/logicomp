package com.alejandrovp.logicomp.service;

import com.alejandrovp.logicomp.dto.DriverDto;
import com.alejandrovp.logicomp.dto.OrderDto;
import com.alejandrovp.logicomp.dto.TruckDto;
import com.alejandrovp.logicomp.entity.DriverEntity;
import com.alejandrovp.logicomp.entity.OrderEntity;
import com.alejandrovp.logicomp.entity.TruckEntity;
import com.alejandrovp.logicomp.mapper.DriverEntityDtoMapper;
import com.alejandrovp.logicomp.mapper.OrderEntityDtoMapper;
import com.alejandrovp.logicomp.persistence.DriverRepository;
import com.alejandrovp.logicomp.persistence.TruckRepository;
import com.alejandrovp.logicomp.util.DriverStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Log
@Transactional
public class DriverService {
    private final DriverRepository driverRepository;
    private final DriverEntityDtoMapper driverEntityDtoMapper;
    private final TruckRepository truckRepository;
    private final OrderService orderService;
    private final OrderEntityDtoMapper orderEntityDtoMapper;

    public Optional<DriverDto> getDriverById(long id) {
        Optional<DriverEntity> result = driverRepository.findById((int) id);
        return result.map(driverEntityDtoMapper::EntityToDto);
    }

    public Collection<DriverDto> getAll() {
        Iterable<DriverEntity> found = driverRepository.findAll();
        Collection<DriverDto> result = new ArrayList<>();

        for(DriverEntity driverEntity : found) {
            result.add(driverEntityDtoMapper.EntityToDto(driverEntity));
        }

        return result;
    }

    public DriverDto createDriver(DriverDto newDriver) {
        DriverEntity created = driverRepository
                .save(driverEntityDtoMapper.DtoToEntity((newDriver)));
        return driverEntityDtoMapper.EntityToDto(created);
    }

    public List<DriverDto> getDriverByName(String name) {
        return driverRepository.findByName(name)
                .stream().map(driverEntityDtoMapper::EntityToDto)
                .toList();
    }

    public boolean driverIsReadyToWork(DriverDto driverDto) {
        DriverEntity driverEntity = driverRepository.findById((int) driverDto.getId()).orElseThrow();
        return driverIsReadyToWork(driverEntity);
    }

    private boolean driverIsReadyToWork(DriverEntity driverEntity) {
        return driverEntity.getWorkingHoursCounter() < driverEntity.getMaxWorkingHours()
                && driverEntity.getAssignedTruck() == null
                && driverEntity.getStatus() == DriverStatus.RESTING;
    }


    public Optional<DriverDto> assignDriverToTruck(DriverDto driverDto, TruckDto truckDto) {
        // Check Truck exists
        Optional<TruckEntity> found = truckRepository.findById((int)truckDto.getId());
        if (found.isEmpty())
            found = truckRepository.findByPlateNumber(truckDto.getPlateNumber());
        TruckEntity truck = found.orElseThrow();
        DriverEntity driver = driverRepository.findById((int)driverDto.getId()).orElseThrow();

        if(driverIsReadyToWork(driver)
                && driver.getCurrentLocation().getId() == truck.getCurrentLocation().getId()) {

            log.info("Assign truck to Driver now");
            driver.setAssignedTruck(truck);
            return Optional.of(driverEntityDtoMapper.EntityToDto(driverRepository.save(driver)));
        }

        return Optional.empty();
    }

    public Collection<DriverDto> getReadyForOrder(long orderId) {
        orderService.getOrderById(orderId).orElseThrow(); // Check existence

        return getAll().stream().filter(driverDto ->
                        (driverDto.getCurrentOrder() == null || driverDto.getCurrentOrder().getId() == orderId)
                                && driverDto.getStatus() == DriverStatus.RESTING
                                && driverDto.getWorkingHoursCounter() < driverDto.getMaxWorkingHours())
                .toList();
    }

    public DriverDto assignDriverToOrder(long driverId, long orderId) {
        OrderEntity order = orderEntityDtoMapper.DtoToEntity(orderService.getOrderById(orderId).orElseThrow());
        DriverEntity driver = driverRepository.findById((int)driverId).orElseThrow();

        driver.setCurrentOrder(order);
        DriverEntity updated = driverRepository.save(driver);

        var tmp = order.getAssignedDrivers();
        tmp.add(updated);
        order.setAssignedDrivers(tmp);
        orderService.update(orderEntityDtoMapper.EntityToDto(order), orderId);

        return driverEntityDtoMapper.EntityToDto(updated);
    }

    public DriverDto changeStatus(DriverDto driverDto, DriverStatus status) {
        DriverEntity driver = driverRepository.findById((int) driverDto.getId()).orElseThrow();
        driver.setStatus(status);
        return driverEntityDtoMapper.EntityToDto(driverRepository.save(driver));
    }
}
