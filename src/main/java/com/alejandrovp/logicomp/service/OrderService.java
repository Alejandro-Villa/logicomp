package com.alejandrovp.logicomp.service;

import com.alejandrovp.logicomp.dto.OrderDto;
import com.alejandrovp.logicomp.dto.WaypointDto;
import com.alejandrovp.logicomp.entity.OrderEntity;
import com.alejandrovp.logicomp.entity.WaypointEntity;
import com.alejandrovp.logicomp.mapper.DriverEntityDtoMapper;
import com.alejandrovp.logicomp.mapper.OrderEntityDtoMapper;
import com.alejandrovp.logicomp.mapper.TruckEntityDtoMapper;
import com.alejandrovp.logicomp.mapper.WaypointEntityDtoMapper;
import com.alejandrovp.logicomp.persistence.DriverRepository;
import com.alejandrovp.logicomp.persistence.OrderRepository;
import com.alejandrovp.logicomp.persistence.TruckRepository;
import com.alejandrovp.logicomp.persistence.WaypointRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Log
@Transactional
public class OrderService {
    private final OrderEntityDtoMapper orderEntityDtoMapper;
    private final OrderRepository orderRepository;
    private final WaypointRepository waypointRepository;
    private final WaypointEntityDtoMapper waypointEntityDtoMapper;
    private final DriverEntityDtoMapper driverEntityDtoMapper;
    private final TruckEntityDtoMapper truckEntityDtoMapper;
    private final TruckRepository truckRepository;
    private final DriverRepository driverRepository;

    public OrderDto createOrder(OrderDto newOrder) {
        return orderEntityDtoMapper.EntityToDto(
                orderRepository.save(orderEntityDtoMapper.DtoToEntity(newOrder)));
    }

    public Optional<OrderDto> getOrderById(long id) {
        return orderRepository.findById((int) id)
                .map(orderEntityDtoMapper::EntityToDto);
    }

    public Collection<OrderDto> getAll() {
        Collection<OrderDto> result = new ArrayList<>();

        orderRepository.findAll().forEach(order ->
                result.add(orderEntityDtoMapper.EntityToDto(order))
        );
        return result;
    }

    public OrderDto update(OrderDto newOrder, long id) {
        OrderEntity updated = orderRepository.findById((int) id)
                .map( order -> {
                    order.setComplete(newOrder.isComplete());
                    if(newOrder.getRoute() != null){
                        WaypointDto[] routeDto = newOrder.getRoute();
                        WaypointEntity[] route = Arrays.stream(routeDto).map(waypointEntityDtoMapper::DtoToEntity).toList().toArray(new WaypointEntity[0]);
                        order.setRoute(route);
                    }
                    if(newOrder.getAssignedDrivers() != null) {
                        order.setAssignedDrivers(
                                newOrder.getAssignedDrivers().stream()
                                        .map(driverEntityDtoMapper::DtoToEntity)
                                        .collect(Collectors.toSet())
                        );
                    }
                    if(newOrder.getAssignedTrucks() != null) {
                        order.setAssignedTrucks(
                                newOrder.getAssignedTrucks().stream()
                                        .map(truckEntityDtoMapper::DtoToEntity)
                                        .collect(Collectors.toSet())
                        );
                    }
                    return orderRepository.save(order);
                })
                .orElseGet(() -> {
                    newOrder.setId(id);
                    OrderEntity toSave = orderEntityDtoMapper.DtoToEntity(newOrder);
                    return orderRepository.save(toSave);
                });
        return orderEntityDtoMapper.EntityToDto(updated);
    }

    public void delete(long id) {
        OrderEntity order = orderRepository.findById((int) id).orElseThrow();
        // First cascade the deletion
        truckRepository.findAll().forEach(
                truckEntity -> {
                    if (truckEntity.getCurrentOrder() != null && truckEntity.getCurrentOrder().equals(order)) {
                        truckEntity.setCurrentOrder(null);
                        truckRepository.save(truckEntity);
                    }
                }
        );

        driverRepository.findAll().forEach(
                driverEntity -> {
                    if(driverEntity.getCurrentOrder() != null && driverEntity.getCurrentOrder().equals(order)) {
                        driverEntity.setCurrentOrder(null);
                        driverRepository.save(driverEntity);
                    }
                }
        );
        // Then delete
        orderRepository.deleteById((int) id);
    }

    public OrderDto addWaypointToOrder(WaypointDto waypointDto, long orderId) {
        // Save the Waypoint
        log.info("Saving " + waypointDto);
        WaypointEntity createdWaypoint = waypointRepository.save(waypointEntityDtoMapper.DtoToEntity(waypointDto));
        OrderEntity order = orderRepository.findById((int) orderId).orElseThrow();

        List<WaypointEntity> route = new ArrayList<>(Arrays.stream(order.getRoute()).toList());
        route.add(createdWaypoint);
        order.setRoute(route.toArray(new WaypointEntity[0]));
        OrderEntity saved = orderRepository.save(order);

        return orderEntityDtoMapper.EntityToDto(saved);
    }
}
