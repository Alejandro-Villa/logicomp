package com.alejandrovp.logicomp.service;

import com.alejandrovp.logicomp.dto.DriverDto;
import com.alejandrovp.logicomp.dto.UserDto;
import com.alejandrovp.logicomp.entity.DriverEntity;
import com.alejandrovp.logicomp.entity.UserEntity;
import com.alejandrovp.logicomp.entity.UserRoleEntity;
import com.alejandrovp.logicomp.mapper.DriverEntityDtoMapper;
import com.alejandrovp.logicomp.mapper.UserEntityDtoMapper;
import com.alejandrovp.logicomp.persistence.DriverRepository;
import com.alejandrovp.logicomp.persistence.UserRepository;
import com.alejandrovp.logicomp.persistence.UserRoleRepository;
import com.alejandrovp.logicomp.security.SecurityTokenManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
@Log
public class AuthService {
    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final DriverRepository driverRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserEntityDtoMapper userEntityDtoMapper;
    private final DriverEntityDtoMapper driverEntityDtoMapper;
    private final SecurityTokenManager securityTokenManager;

    public String signup(UserDto newUser, long driverId) {
        String hash = passwordEncoder.encode(newUser.getPassword());
        newUser.setPassword(hash);

        if(userRepository.findByUsername(newUser.getUsername()).isPresent())
            return null; // User already exists, should do a login

        UserEntity created =
                userRepository.save(userEntityDtoMapper.DtoToEntity(newUser));

        // FIXME: don't make two queries
        created.setRoles(getDefaultRoles());

        if(driverId > 0) {
            Optional<DriverEntity> optionalDriver = driverRepository.findById((int) driverId);
            if(optionalDriver.isPresent()) {
                DriverEntity driverEntity = optionalDriver.get();
                created.setDriver(driverEntity);
                addRoleToUser(created.getUsername(), "DRIVER");
            }
            else
                log.info("Could not register Driver with Id: " + driverId + " with user: " + created.getUsername());
        }

        userRepository.save(created);

        return securityTokenManager.createToken(created.getUsername());
    }

    public String signup(UserDto newUser) {
        return signup(newUser, -1);
    }

    public Collection<UserRoleEntity> getDefaultRoles() {
        List<UserRoleEntity> roles = new ArrayList<>();
        roles.add(userRoleRepository.findByTextcode("ROLE_USER").orElseThrow());
        return roles;
    }

    public String login(UserDto user) {
        Optional<UserEntity> optFound =
                userRepository.findByUsername(user.getUsername());
        if(optFound.isEmpty())
            return null;
        UserEntity found = optFound.orElseThrow();
        if(!passwordEncoder.matches(user.getPassword(), found.getPassword()))
            throw new RuntimeException("Password doesn't match for user " + user.getUsername());

        return securityTokenManager.createToken(user.getUsername());
    }

    public void addRoleToUser(String username, String role) {
        Optional<UserEntity> optionalUser = userRepository.findByUsername(username);
        UserEntity user = optionalUser.orElseThrow();

        Collection<UserRoleEntity> actualRoles = user.getRoles();
        UserRoleEntity roleEntity = userRoleRepository.findByTextcode("ROLE_" + role).orElseThrow();

        if(!actualRoles.contains(roleEntity)) {
            actualRoles.add(roleEntity);
            user.setRoles(actualRoles);
            userRepository.save(user);
        }
    }

    public Optional<DriverDto> getDriverOfUser(String username) {
        Optional<UserEntity> user = userRepository.findByUsername(username);
        if(user.isPresent() && user.get().isDriver()) {
            return driverRepository.findById((int) user.get().getDriver().getId()).map(driverEntityDtoMapper::EntityToDto);
        }
        return Optional.empty();
    }
}
