package com.alejandrovp.logicomp.service;

import com.alejandrovp.logicomp.dto.OrderDto;
import com.alejandrovp.logicomp.dto.TruckDto;
import com.alejandrovp.logicomp.entity.CityEntity;
import com.alejandrovp.logicomp.entity.DriverEntity;
import com.alejandrovp.logicomp.entity.OrderEntity;
import com.alejandrovp.logicomp.entity.TruckEntity;
import com.alejandrovp.logicomp.mapper.CityEntityDtoMapper;
import com.alejandrovp.logicomp.mapper.DriverEntityDtoMapper;
import com.alejandrovp.logicomp.mapper.OrderEntityDtoMapper;
import com.alejandrovp.logicomp.mapper.TruckEntityDtoMapper;
import com.alejandrovp.logicomp.persistence.OrderRepository;
import com.alejandrovp.logicomp.persistence.TruckRepository;
import com.alejandrovp.logicomp.util.PlateNumber;
import com.alejandrovp.logicomp.util.TruckState;
import com.alejandrovp.logicomp.util.WaypointType;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Log
@RequiredArgsConstructor
public class TruckService {
    private final TruckEntityDtoMapper truckEntityDtoMapper;
    private final TruckRepository truckRepository;
    private final OrderService orderService;
    private final CityService cityService;
    private final DriverService driverService;
    private final DriverEntityDtoMapper driverEntityDtoMapper;
    private final CityEntityDtoMapper cityEntityDtoMapper;
    private final OrderEntityDtoMapper orderEntityDtoMapper;
    private final OrderRepository orderRepository;

    public Optional<TruckDto> getTruckById(long id) {
        return truckRepository.findById((int) id)
                .map(truckEntityDtoMapper::EntityToDto);
    }

    public Optional<TruckDto> getTruckByPlateNumber(PlateNumber plateNumber) {
        return truckRepository.findByPlateNumber(plateNumber)
                .map(truckEntityDtoMapper::EntityToDto);
    }

    public Collection<TruckDto> getAll() {
        Collection<TruckDto> result = new ArrayList<>();
        truckRepository.findAll().forEach(truck ->
                result.add(truckEntityDtoMapper.EntityToDto(truck))
        );
        return result;
    }

    public TruckDto createTuck(TruckDto newTruck) {
        TruckEntity created = truckRepository.save(truckEntityDtoMapper.DtoToEntity(newTruck));
        return truckEntityDtoMapper.EntityToDto(created);
    }

    public Collection<TruckDto> getReadyForOrder(long orderId) {
        OrderDto order = orderService.getOrderById(orderId).orElseThrow();
        int totalCapacity = Arrays.stream(order.getRoute()).map(waypointDto -> {
            if(waypointDto.getType() == WaypointType.LOAD)
                return waypointDto.getCargo().getWeightKg();
            else
                return waypointDto.getCargo().getWeightKg() * -1;
        }).reduce(Integer::sum).orElse(0);

        Collection<TruckDto> result = new ArrayList<>();
        truckRepository.findAll().forEach(
                truckEntity -> {
                    if( truckEntity.getCurrentOrder() == null &&
                            truckEntity.getState() == TruckState.OK &&
                            (truckEntity.getMaxCapacityTons() * 1000) >= totalCapacity)
                        result.add(truckEntityDtoMapper.EntityToDto(truckEntity));
                }
        );
        return result;
    }

    public TruckDto update(TruckDto newTruck, long id) {
        TruckEntity updated = truckRepository.findById((int) id)
                .map(
                        truck -> {
                            if(newTruck.getDrivers() != null) {
                                List<DriverEntity> drivers = newTruck.getDrivers().stream()
                                        .map(driverDto -> driverService.getDriverById(driverDto.getId())
                                                .map(driverEntityDtoMapper::DtoToEntity))
                                        .filter(Optional::isPresent).map(Optional::get).toList();
                                truck.setDrivers(drivers);
                            }
                            truck.setState(newTruck.getState());
                            CityEntity city = cityEntityDtoMapper.DtoToEntity(
                                    cityService.getCityById(newTruck.getCurrentLocation().getId()).orElseThrow());
                            truck.setCurrentLocation(city);
                            truck.setMaxCapacityTons(newTruck.getMaxCapacityTons());
                            truck.setPlateNumber(newTruck.getPlateNumber());
                            if(newTruck.getCurrentOrder() != null) {
                                OrderEntity order = orderEntityDtoMapper.DtoToEntity(
                                        orderService.getOrderById(newTruck.getCurrentOrder().getId()).orElseThrow()
                                );
                                truck.setCurrentOrder(order);
                            }
                            log.info("Updating with \n" + truck);
                            return truckRepository.save(truck);
                        }
                )
                .orElseGet(() -> {
                    newTruck.setId(id);
                    TruckEntity toSave = truckEntityDtoMapper.DtoToEntity(newTruck);
                    return truckRepository.save(toSave);
                });
        return truckEntityDtoMapper.EntityToDto(updated);
    }

    public void delete(long id) {
        truckRepository.deleteById((int)id);
    }

    public TruckDto assignTruckToOrder(long truckId, long orderId) {
        TruckEntity truck = truckRepository.findById((int) truckId).orElseThrow();
        OrderEntity order = orderRepository.findById((int) orderId).orElseThrow();

        truck.setCurrentOrder(order);
        TruckEntity updated = truckRepository.save(truck);
        Set<TruckEntity> actual = order.getAssignedTrucks();
        actual.add(updated);
        order.setAssignedTrucks(actual);
        orderRepository.save(order);
        return truckEntityDtoMapper.EntityToDto(updated);
    }
}
