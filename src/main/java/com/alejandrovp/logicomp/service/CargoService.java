package com.alejandrovp.logicomp.service;

import com.alejandrovp.logicomp.dto.CargoDto;
import com.alejandrovp.logicomp.entity.CargoEntity;
import com.alejandrovp.logicomp.mapper.CargoEntityDtoMapper;
import com.alejandrovp.logicomp.persistence.CargoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CargoService {
    private final CargoEntityDtoMapper cargoEntityDtoMapper;
    private final CargoRepository cargoRepository;

    public CargoDto createCargo(CargoDto cargoDto) {
        CargoEntity cargoEntity = cargoEntityDtoMapper.DtoToEntity(cargoDto);
        CargoEntity persisted   = cargoRepository.save(cargoEntity);
        return cargoEntityDtoMapper.EntityToDto(persisted);
    }

    public Optional<CargoDto> getCargoById(long id) {
        Optional<CargoEntity> result = cargoRepository.findById((int) id);
        return result.map(cargoEntityDtoMapper::EntityToDto);
    }
    public List<CargoDto> getCargoByName(String name) {
        return cargoRepository.findByName(name)
                .stream().map(cargoEntityDtoMapper::EntityToDto)
                .collect(Collectors.toList());
    }

    public Collection<CargoDto> getAll() {
        var response = cargoRepository.findAll();
        Collection<CargoDto> result = new ArrayList<>();

        for(CargoEntity cargoEntity : response) {
            result.add(cargoEntityDtoMapper.EntityToDto(cargoEntity));
        }
        return result;
    }

    public CargoDto update(CargoDto newCargo, long id) {
        CargoEntity updated = cargoRepository.findById((int) id)
                .map(cargo -> {
                    cargo.setName(newCargo.getName());
                    cargo.setDescription(newCargo.getDescription());
                    cargo.setWeightKg(newCargo.getWeightKg());
                    cargo.setStatus(newCargo.getStatus());
                    return cargoRepository.save(cargo);
                })
                .orElseGet(() -> {
                    newCargo.setId(id);
                    CargoEntity toSave = cargoEntityDtoMapper.DtoToEntity(newCargo);
                    return cargoRepository.save(toSave);
                });
        return cargoEntityDtoMapper.EntityToDto(updated);
    }

    public void delete(long id) {
        cargoRepository.deleteById((int) id);
    }
}
