package com.alejandrovp.logicomp.service;

import com.alejandrovp.logicomp.dto.CityDto;
import com.alejandrovp.logicomp.entity.CityEntity;
import com.alejandrovp.logicomp.mapper.CityEntityDtoMapper;
import com.alejandrovp.logicomp.persistence.CityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class CityService {
    private final CityEntityDtoMapper cityEntityDtoMapper;
    private final CityRepository cityRepository;

    public CityDto createCity(CityDto cityDto) {
        CityEntity created = cityRepository
                .save(cityEntityDtoMapper.DtoToEntity(cityDto));
        return cityEntityDtoMapper.EntityToDto(created);
    }

    public Optional<CityDto> getCityById(long id) {
        return cityRepository.findById((int) id)
                .map(cityEntityDtoMapper::EntityToDto);
    }

    public Optional<CityDto> getCityByName(String name) {
        return cityRepository.findByName(name)
                .map(cityEntityDtoMapper::EntityToDto);
    }

    public Collection<CityDto> getAll() {
        Iterable<CityEntity> response = cityRepository.findAll();
        Collection<CityDto> result = new ArrayList<>();

        for(CityEntity cityEntity : response) {
            result.add(cityEntityDtoMapper.EntityToDto(cityEntity));
        }
        return result;
    }

    public CityDto update(CityDto newCity, long id) {
        /*
         * When mapping Driver and Truck DTO to Entity,
         * we need to make sure they EXIST in the DB,
         * or we create incoherent information.
         */
        CityEntity updated = cityRepository.findById((int) id)
                .map(city -> {
                    city.setName(newCity.getName());
                    // FIXME
                    city.setDrivers(null);
                    // FIXME
                    city.setTrucks(null);
                    return cityRepository.save(city);
                })
                .orElseGet(() -> {
                    newCity.setId(id);
                    CityEntity toSave = cityEntityDtoMapper.DtoToEntity(newCity);
                    return cityRepository.save(toSave);
                });
        return cityEntityDtoMapper.EntityToDto(updated);
    }

    public void delete(long id) {
        cityRepository.deleteById((int) id);
    }
}
