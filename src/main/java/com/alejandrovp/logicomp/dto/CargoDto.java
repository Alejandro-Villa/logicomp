package com.alejandrovp.logicomp.dto;

import com.alejandrovp.logicomp.util.CargoStatus;
import lombok.Data;

@Data
public class CargoDto {
    private long id;
    private String name;
    private String description;
    private int weightKg;

    private CargoStatus status;
}