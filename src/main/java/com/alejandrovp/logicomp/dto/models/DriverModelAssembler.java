package com.alejandrovp.logicomp.dto.models;

import com.alejandrovp.logicomp.controller.DriverController;
import com.alejandrovp.logicomp.dto.DriverDto;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class DriverModelAssembler implements RepresentationModelAssembler<DriverDto, EntityModel<DriverDto>> {
    @Override
    public EntityModel<DriverDto> toModel(DriverDto driverDto) {
        return EntityModel.of(driverDto,
               linkTo(methodOn(DriverController.class).one(driverDto.getId())).withSelfRel(),
                linkTo(methodOn(DriverController.class).all()).withRel("all"));
    }
}
