package com.alejandrovp.logicomp.dto.models;

import com.alejandrovp.logicomp.controller.CargoController;
import com.alejandrovp.logicomp.dto.CargoDto;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class CargoModelAssembler implements RepresentationModelAssembler<CargoDto, EntityModel<CargoDto>> {

    @Override
    public EntityModel<CargoDto> toModel(CargoDto cargoDto) {
        return EntityModel.of(cargoDto,
                linkTo(methodOn(CargoController.class).one(cargoDto.getId())).withSelfRel(),
                linkTo(methodOn((CargoController.class)).all()).withRel("all"));
    }
}
