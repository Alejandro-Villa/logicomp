package com.alejandrovp.logicomp.dto.models;

import com.alejandrovp.logicomp.controller.TruckController;
import com.alejandrovp.logicomp.dto.TruckDto;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class TruckModelAssembler implements RepresentationModelAssembler<TruckDto, EntityModel<TruckDto>> {
    @Override
    public EntityModel<TruckDto> toModel(TruckDto entity) {
        return EntityModel.of(entity,
                linkTo(methodOn(TruckController.class).one(entity.getId())).withSelfRel(),
                linkTo(methodOn(TruckController.class).all()).withRel("all"));
    }
}
