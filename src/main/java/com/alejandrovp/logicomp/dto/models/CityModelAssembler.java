package com.alejandrovp.logicomp.dto.models;


import com.alejandrovp.logicomp.controller.CityController;
import com.alejandrovp.logicomp.dto.CityDto;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class CityModelAssembler implements RepresentationModelAssembler<CityDto, EntityModel<CityDto>> {
    @Override
    public EntityModel<CityDto> toModel(CityDto cityDto) {
        return EntityModel.of(cityDto,
                linkTo(methodOn(CityController.class).one(cityDto.getId())).withSelfRel(),
                linkTo(methodOn(CityController.class).all()).withRel("all"));
    }
}
