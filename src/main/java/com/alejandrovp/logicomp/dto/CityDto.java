package com.alejandrovp.logicomp.dto;

import lombok.Data;

import java.util.Set;

@Data
public class CityDto {
    private long id;
    private String name;
    private Set<TruckDto> trucks;
    private Set<DriverDto> drivers;
}