package com.alejandrovp.logicomp.dto;

import com.alejandrovp.logicomp.util.DriverStatus;
import lombok.Data;

@Data
public class DriverDto {
    private long id;
    private String name;
    private String surname;

    private int workingHoursCounter;
    private static final int maxWorkingHours = 176;

    private DriverStatus status;
    private CityDto currentLocation;
    private TruckDto assignedTruck;
    private OrderDto currentOrder;
}