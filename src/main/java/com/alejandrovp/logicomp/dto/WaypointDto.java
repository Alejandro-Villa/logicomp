package com.alejandrovp.logicomp.dto;

import com.alejandrovp.logicomp.util.WaypointType;
import lombok.Data;

@Data
public class WaypointDto {
    private long id;
    private CityDto location;
    private CargoDto cargo;
    private WaypointType type;
}