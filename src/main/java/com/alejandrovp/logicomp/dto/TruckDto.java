package com.alejandrovp.logicomp.dto;

import com.alejandrovp.logicomp.util.PlateNumber;
import com.alejandrovp.logicomp.util.TruckState;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.lang.Nullable;

import java.util.List;

@Data
public class TruckDto {
    private long id;
    private PlateNumber plateNumber;
    private int maxCapacityTons;
    private TruckState state;
    private String cityName;
    private CityDto currentLocation;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Nullable
    private List<DriverDto> drivers;
    @Nullable
    private OrderDto currentOrder;
}