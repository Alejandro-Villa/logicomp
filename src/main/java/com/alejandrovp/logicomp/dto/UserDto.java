package com.alejandrovp.logicomp.dto;

import lombok.Data;

@Data
public class UserDto {
    private long id;
    private String username;
    private String password;
    private DriverDto driver;
    public  boolean isDriver() { return driver != null;}
}
