package com.alejandrovp.logicomp.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Set;

@Data
public class OrderDto {
    private long id;
    private boolean complete;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<TruckDto> assignedTrucks;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<DriverDto> assignedDrivers;
    private WaypointDto[] route;
}