package com.alejandrovp.logicomp.controller;

import com.alejandrovp.logicomp.dto.TruckDto;
import com.alejandrovp.logicomp.dto.models.TruckModelAssembler;
import com.alejandrovp.logicomp.service.TruckService;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(path = "/api/truck")
@RequiredArgsConstructor
public class TruckController {
    private final TruckService truckService;
    private final TruckModelAssembler truckModelAssembler;

    @GetMapping("/truck/{id}")
    @Secured("ROLE_DRIVER")
    public @ResponseBody EntityModel<TruckDto> one(@PathVariable long id) {
        return truckModelAssembler.toModel(
                truckService.getTruckById(id).orElseThrow()
        );
    }

    @GetMapping("/all")
    @Secured("ROLE_MANAGER")
    public CollectionModel<EntityModel<TruckDto>> all() {
        List<EntityModel<TruckDto>> found = truckService.getAll().stream()
                .map(truckModelAssembler::toModel)
                .toList();
        return CollectionModel.of(found, linkTo(methodOn(TruckController.class).all()).withSelfRel());
    }

    @PostMapping("/new")
    @Secured("ROLE_MANAGER")
    public ResponseEntity<?> add(@RequestBody TruckDto newTruck) {
        EntityModel<TruckDto> model = truckModelAssembler.toModel(
                truckService.createTuck(newTruck)
        );

        return ResponseEntity
                .created(model.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(model);
    }
}
