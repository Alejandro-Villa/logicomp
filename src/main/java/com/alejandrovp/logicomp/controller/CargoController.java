package com.alejandrovp.logicomp.controller;

import com.alejandrovp.logicomp.dto.CargoDto;
import com.alejandrovp.logicomp.dto.models.CargoModelAssembler;
import com.alejandrovp.logicomp.service.CargoService;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(path = "/api/cargo")
@RequiredArgsConstructor
public class CargoController {
    // Following https://spring.io/guides/tutorials/rest/
    private final CargoService cargoService;
    private final CargoModelAssembler cargoModelAssembler;

    @Secured({"ROLE_MANAGER"})
    @PostMapping({"/", "/new"})
    public ResponseEntity<?> add(@RequestBody CargoDto newCargo) {
        CargoDto created = cargoService.createCargo(newCargo);
        EntityModel<CargoDto> createdModel = cargoModelAssembler.toModel(created);

        return ResponseEntity
                .created(createdModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(createdModel);
    }

    // tag::get-aggregate-root[]
    @Secured({"ROLE_MANAGER"})
    @GetMapping({"/", "/all"})
    public CollectionModel<EntityModel<CargoDto>> all(){
        List<EntityModel<CargoDto>> found = cargoService.getAll().stream()
                .map(cargoModelAssembler::toModel)
                .toList();
        return CollectionModel.of(found, linkTo(methodOn(CargoController.class).all()).withSelfRel());
    }
    // end::get-aggregate-root[]
    @Secured({"ROLE_MANAGER"})
    @GetMapping({"/{id}", "/id/{id}"})
    public @ResponseBody EntityModel<CargoDto> one(@PathVariable long id) {
        CargoDto found = cargoService.getCargoById(id).orElseThrow();
        return cargoModelAssembler.toModel(found);
    }

    @Secured({"ROLE_MANAGER"})
    @PutMapping("/cargo/{id}")
    public ResponseEntity<?> update(@RequestBody CargoDto newCargo, @PathVariable long id) {
        CargoDto updatedCargo = cargoService.update(newCargo, id);

        EntityModel<CargoDto> model = cargoModelAssembler.toModel(updatedCargo);

        return ResponseEntity
                .created(model.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(model);
    }

    @Secured({"ROLE_MANAGER"})
    @DeleteMapping("/cargo/{id}")
    public ResponseEntity<?> delete(@PathVariable long id) {
        cargoService.delete(id);

        return ResponseEntity.noContent().build();
    }
}
