package com.alejandrovp.logicomp.controller;

import com.alejandrovp.logicomp.dto.CityDto;
import com.alejandrovp.logicomp.dto.models.CityModelAssembler;
import com.alejandrovp.logicomp.service.CityService;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(path = "/api/city")
@RequiredArgsConstructor
public class CityController {

    private final CityService cityService;

    private final CityModelAssembler cityModelAssembler;

    @Secured({"ROLE_ADMIN"})
    @PostMapping("/new")
    public ResponseEntity<?> add(CityDto newCity) {
        CityDto created = cityService.createCity(newCity);
        EntityModel<CityDto> model = cityModelAssembler.toModel(created);

        return ResponseEntity
                .created(model.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(model);
    }

    @Secured({"ROLE_DRIVER", "ROLE_MANAGER"})
    @GetMapping("/city/{id}")
    public @ResponseBody EntityModel<CityDto> one(@PathVariable long id) {
        CityDto found = cityService.getCityById(id).orElseThrow();
        return cityModelAssembler.toModel(found);
    }

    @Secured({"ROLE_DRIVER", "ROLE_MANAGER"})
    @GetMapping("/all")
    public CollectionModel<EntityModel<CityDto>> all() {
        List<EntityModel<CityDto>> found = cityService.getAll().stream()
                .map(cityModelAssembler::toModel)
                .toList();
        return CollectionModel.of(found,
                linkTo(methodOn(CityController.class).all()).withSelfRel());
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/city/{id}")
    public ResponseEntity<?> update(@RequestBody CityDto newCity, @PathVariable long id) {
        CityDto updatedCity = cityService.update(newCity, id);

        EntityModel<CityDto> model = cityModelAssembler.toModel(updatedCity);

        return ResponseEntity
                .created(model.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(model);
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/city/{id}")
    public ResponseEntity<?> delete(@PathVariable long id) {
        cityService.delete(id);

        return ResponseEntity.noContent().build();
    }
}
