package com.alejandrovp.logicomp.controller;

import com.alejandrovp.logicomp.dto.DriverDto;
import com.alejandrovp.logicomp.dto.models.DriverModelAssembler;
import com.alejandrovp.logicomp.service.DriverService;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(path = "/api/driver")
@RequiredArgsConstructor
public class DriverController {
    private final DriverService driverService;
    private final DriverModelAssembler driverModelAssembler;

    @Secured("ROLE_DRIVER")
    @GetMapping("/driver/{id}")
    // TODO: Use @PostAuthorize and Spring-EL to only let Principal access its own info
    public @ResponseBody EntityModel<DriverDto> one(@PathVariable long id) {
        DriverDto found = driverService.getDriverById(id).orElseThrow();
        return driverModelAssembler.toModel(found);
    }

    @Secured("ROLE_MANAGER")
    @GetMapping("/all")
    public CollectionModel<EntityModel<DriverDto>> all() {
        List<EntityModel<DriverDto>> found = driverService.getAll().stream()
                .map(driverModelAssembler::toModel)
                .toList();
        return CollectionModel.of(found, linkTo(methodOn(DriverController.class).all()).withSelfRel());
    }

    @PostMapping("/driver/{id}")
    @Secured("ROLE_DRIVER")
    public ResponseEntity<?> add(@RequestBody DriverDto newDriver) {
        EntityModel<DriverDto> created = EntityModel.of(driverService.createDriver(newDriver));

        return ResponseEntity
                .created(created.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(created);
    }
}
