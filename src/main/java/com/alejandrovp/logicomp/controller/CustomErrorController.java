package com.alejandrovp.logicomp.controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.java.Log;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Log
@Controller
public class CustomErrorController implements ErrorController {
    @RequestMapping("/error")
    public String handleError(HttpServletRequest request, Model model) {
        int errorCode = Integer.parseInt(request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE).toString());
        String errorMsg = request.getAttribute(RequestDispatcher.ERROR_MESSAGE).toString();
        model.addAttribute("errorCode", errorCode);
        model.addAttribute("errorMsg", errorMsg);

        if(errorCode == 403)
            return "errors/403";

        return "errors/generic-error";
    }
}
