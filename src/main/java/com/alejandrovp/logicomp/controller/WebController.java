package com.alejandrovp.logicomp.controller;

import com.alejandrovp.logicomp.dto.*;
import com.alejandrovp.logicomp.service.*;
import com.alejandrovp.logicomp.util.CargoStatus;
import com.alejandrovp.logicomp.util.DriverStatus;
import com.alejandrovp.logicomp.util.TruckState;
import com.alejandrovp.logicomp.util.WaypointType;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
@Log
public class WebController {
    private final CargoService cargoService;
    private final TruckService truckService;
    private final OrderService orderService;
    private final DriverService driverService;
    private final CityService cityService;
    private final AuthService authService;

    @GetMapping("/")
    public String index(Model model) {
        return "index";
    }

    @GetMapping("/userArea")
    @Secured("ROLE_USER")
    public String userAreaView(Model model) {
        return "userArea";
    }

    @GetMapping({"/cargo", "/cargo/"})
    @Secured("ROLE_MANAGER")
    public String cargoView(Model model) {
        List<CargoDto> allCargo = cargoService.getAll().stream().toList();

        model.addAttribute("cargoList", allCargo);
        return "views/cargo";
    }

    @GetMapping({"/cargo/add"})
    public String cargoAddForm(CargoDto cargo, Model model) {
        model.addAttribute("cargo", cargo);
        model.addAttribute("cargoStatus", CargoStatus.values());
        return "views/cargo-add";
    }
    @PostMapping({"/cargo/add"})
    public String cargoAdd(@Validated CargoDto cargo, BindingResult result, Model model) {
        if(result.hasErrors())
            return "views/cargo-add";

        cargoService.createCargo(cargo);
        return "redirect:/cargo";
    }
    @GetMapping({"/cargo/edit/{id}"})
    public String cargoUpdateForm(@PathVariable("id") long id, Model model) {
        CargoDto cargoDto = cargoService.getCargoById(id).orElseThrow(() -> new IllegalArgumentException("Invalid Cargo id: " + id));
        model.addAttribute("cargo", cargoDto);
        model.addAttribute("cargoStatus", CargoStatus.values());
        return "views/cargo-update";
    }

    @PostMapping({"/cargo/update/{id}"})
    public String cargoUpdate(@PathVariable("id") long id, @Validated CargoDto cargo, BindingResult result, Model model) {
        if(result.hasErrors()) {
            cargo.setId(id);
            return "views/cargo-update";
        }

        cargoService.update(cargo, id);
        return "redirect:/cargo";
    }

    @GetMapping({"/cargo/delete/{id}"})
    public String cargoDelete(@PathVariable("id") long id) {
        cargoService.delete(id);
        return "redirect:/cargo";
    }

    @GetMapping(value = {"/truck", "/truck/"})
    public String truckView(@RequestParam(value = "orderId", required = false) Optional<Long> orderId, Model model) {
            List<TruckDto> truckList;
        truckList = orderId
                .map(id -> truckService.getReadyForOrder(id).stream().toList())
                .orElseGet(() -> truckService.getAll().stream().toList());
            model.addAttribute("truckList", truckList);
            model.addAttribute("truckState", TruckState.values());
            model.addAttribute("orderId", orderId);

            return "views/truck";
    }

    @GetMapping("/truck/add")
    public String addTruckView(TruckDto truckDto, Model model) {
        model.addAttribute("truck", truckDto);
        model.addAttribute("truckStatus", TruckState.values());

        return "views/truck-add";
    }

    @PostMapping("/truck/add")
    public String addTruckForm(@Validated TruckDto truckDto, BindingResult result, Model model) {
        Optional<CityDto> found = cityService.getCityByName(truckDto.getCityName());
        if(found.isPresent()) {
            truckDto.setCurrentLocation(found.get());
        }
        else {
            result.rejectValue("currentLocation", "City " + truckDto.getCityName() + " not found");
        }

        if(truckService.getTruckByPlateNumber(truckDto.getPlateNumber()).isPresent()) {
            result.rejectValue("plateNumber", "Plate Number " + truckDto.getPlateNumber() + " already registered");
        }

        if(result.hasErrors()) {
            model.addAttribute("truck", truckDto);
            model.addAttribute("truckStatus", TruckState.values());
            return "views/truck-add";
        }

        truckDto.setDrivers(new ArrayList<>());
        truckDto.setCurrentOrder(null);
        truckService.createTuck(truckDto);

        return "redirect:/truck";
    }

    @GetMapping("/truck/edit/{id}")
    public String updateTruckView(@PathVariable("id") long id, TruckDto truckDto, Model model) {
        truckDto = truckService.getTruckById(id).orElseThrow();

        model.addAttribute("truck", truckDto);
        model.addAttribute("truckStatus", TruckState.values());

        return "views/truck-update";
    }

    @PostMapping("/truck/update/{id}")
    public String updateTruck(@PathVariable("id") long id, @Validated TruckDto truckDto, BindingResult result, Model model) {
        String cityName = (String) result.getFieldValue("currentLocation");
        CityDto cityDto = null; // Should never trigger NullPointerException

        //if(cityName != null && !cityName.isEmpty())
        Optional<CityDto> found = cityService.getCityByName(cityName);
        if(found.isPresent()) {
            cityDto = found.get();

            // Clean the City-related errors
            //TODO: Shouldn't do this, is viewed as bad practice
            List<FieldError> errorsToKeep = result.getFieldErrors().stream()
                    .filter(fieldError -> !fieldError.getField().equals("currentLocation"))
                    .toList();
            result = new BeanPropertyBindingResult(truckDto,"truck");
            errorsToKeep.forEach(result::addError);
        }
        else
            result.rejectValue("currentLocation", "City " + cityName + " not found");

        if(result.hasErrors()) {
            truckDto.setId(id);
            model.addAttribute("truck", truckDto);
            model.addAttribute("truckStatus", TruckState.values());
            return "views/truck-update";
        }

        truckDto.setCurrentLocation(cityDto);
        truckDto.setDrivers(new ArrayList<>());
        truckDto.setCurrentOrder(null);
        truckService.update(truckDto, id);

        return "redirect:/truck";
    }

    @GetMapping("/truck/delete/{id}")
    public String truckDelete(@PathVariable("id") long id) {
        truckService.delete(id);
        return "redirect:/truck";
    }

    @GetMapping({"/order", "/order/"})
    public String orderView(Model model) {
        List<OrderDto> allOrders = orderService.getAll().stream().toList();

        model.addAttribute("orderList", allOrders);

        return "views/order";
    }

    @GetMapping("/order/{id}/assignTruck")
    public String trucksAvailableForOrder(@PathVariable("id") long orderId, Model model) {
        List<TruckDto> truckDtoList = truckService.getReadyForOrder(orderId).stream().toList();

        OrderDto order = orderService.getOrderById(orderId).orElseThrow(() -> new RuntimeException("Invalid Order id " + orderId));
        model.addAttribute("order", order);
        model.addAttribute("truckList", truckDtoList);

        return "views/order-assignTruck";
    }

    @GetMapping("/truck/addToOrder/{truckId}/{orderId}")
    public String assignTruckToOrder(@PathVariable("truckId") long truckId, @PathVariable("orderId") long orderId) {
        truckService.assignTruckToOrder(truckId, orderId);

        return "redirect:/order/" + orderId + "/assignTruck";
    }

    @GetMapping("/order/{id}/assignDriver")
    public String driverAvailableForOrder(@PathVariable("id") long orderId, Model model) {
        List<DriverDto> driverDtoList = driverService.getReadyForOrder(orderId).stream().toList();

        OrderDto order = orderService.getOrderById(orderId).orElseThrow(() -> new RuntimeException("Invalid Order id " + orderId));
        model.addAttribute("order", order);
        model.addAttribute("driverList", driverDtoList);

        return "views/order-assignDriver";
    }

    @GetMapping("/driver/addToOrder/{driverId}/{orderId}")
    public String assignDriverToOrder(@PathVariable("driverId") long driverId, @PathVariable("orderId") long orderId) {
        driverService.assignDriverToOrder(driverId, orderId);

        return "redirect:/order/" + orderId + "/assignDriver";
    }

    @GetMapping("/order/{id}/addWaypoint")
    public String orderAddWaypoint(@PathVariable("id") long id, WaypointDto waypointDto, Model model) {
        OrderDto orderDto = orderService.getOrderById(id).orElseThrow(() -> new RuntimeException("Invalid Order id " + id));
        List<CityDto> cityList = cityService.getAll().stream().toList();
        List<CargoDto> cargoList = cargoService.getAll().stream().toList();

        model.addAttribute("order", orderDto);
        model.addAttribute("cityList", cityList);
        model.addAttribute("cargoList", cargoList);
        model.addAttribute("waypoint", waypointDto);
        model.addAttribute("waypointType", WaypointType.values());

        return "views/order-addWaypoint";
    }

    @PostMapping("/waypoint/addToOrder/{orderId}")
    public String orderWaypointForm(@PathVariable("orderId") long orderId, @Validated WaypointDto waypointDto, BindingResult result, Model model) {
        log.info("Received " + waypointDto);
        long cityId = Long.parseLong((String) result.getFieldValue("location"));
        long cargoId = Long.parseLong((String) result.getFieldValue("cargo"));

        result = new BeanPropertyBindingResult(waypointDto, "waypoint");

        try {
            waypointDto.setLocation(
                    cityService.getCityById(cityId).orElseThrow(NoSuchElementException::new)
            );
            waypointDto.setCargo(
                    cargoService.getCargoById(cargoId).orElseThrow(NoSuchElementException::new)
            );
        } catch (Exception e) {
            result.addError(new ObjectError("waypoint", e.getMessage()));
        }

        if(waypointDto.getType() == null) {
            result.rejectValue("type", "Type cannot be empty");
        }

        if(result.hasErrors()) {
            OrderDto orderDto = orderService.getOrderById(orderId).orElseThrow();
            List<CityDto> cityList = cityService.getAll().stream().toList();
            List<CargoDto> cargoList = cargoService.getAll().stream().toList();

            model.addAttribute("order", orderDto);
            model.addAttribute("cityList", cityList);
            model.addAttribute("cargoList", cargoList);
            model.addAttribute("waypoint", waypointDto);
            model.addAttribute("waypointType", WaypointType.values());
            return "views/order-addWaypoint";
        }

        log.info("Sending " + waypointDto);
        orderService.addWaypointToOrder(waypointDto, orderId);

        return "redirect:/order/" + orderId + "/addWaypoint";
    }
    @GetMapping("/order/delete/{id}")
    public String deleteOrder(@PathVariable("id") long orderId) {
        orderService.delete(orderId);
        return "redirect:/order";
    }

    @GetMapping("/order/add")
    public String addOrder() {
        OrderDto orderDto = new OrderDto();
        orderDto.setComplete(false);
        orderService.createOrder(orderDto);

        return "redirect:/order";
    }
   @GetMapping("/driver/panel/{user}")
   @Secured("ROLE_DRIVER")
   public String driverControlPanelView(@PathVariable("user") String user, Model model) {
       return authService.getDriverOfUser(user).map(
                       driverDto -> {
                           model.addAttribute("driver", driverDto);
                           return "views/driver-panel";
                       }
               )
               .orElse(("/"));
   }

   @Secured("ROLE_DRIVER")
   @PostMapping("/driver/{id}/changeStatus")
   public String updateStatus(@PathVariable("id") long driverId, BindingResult result, @Validated DriverDto driverDto) {
        if(!result.hasErrors()) {
            DriverDto updated = driverService.changeStatus(driverDto, driverDto.getStatus());
        }

        return "redirect:/driver/panel/" + driverDto.getName();
   }
}
