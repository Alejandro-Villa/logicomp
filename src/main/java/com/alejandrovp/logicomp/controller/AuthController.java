package com.alejandrovp.logicomp.controller;

import com.alejandrovp.logicomp.dto.UserDto;
import com.alejandrovp.logicomp.service.AuthService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
@RequiredArgsConstructor
@Log
public class AuthController {
    private final AuthService authService;

    @PostMapping("/auth/signup")
    public ResponseEntity<?> signup(@RequestBody UserDto newUser) {
        String res = authService.signup(newUser);
        // Return token here inside body
        return ResponseEntity.ok(res);
    }

    @PostMapping("/auth/login")
    public ResponseEntity<?> login(@RequestBody UserDto user) {
        String res = authService.login(user);

        if(res == null || res.isEmpty())
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok(res);
    }

    // Web views for loging/signup pages
    @GetMapping("/auth/signup")
    public String signupView(Model model) {
        model.addAttribute("user", new UserDto());
        return "signup";
    }
    @GetMapping("/auth/login")
    public String loginView(Model model) {
        model.addAttribute("user", new UserDto());
        return "login";
    }

    @PostMapping(
            path = "/auth/web/login",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<String> handleWebLogin(UserDto user, HttpServletResponse response) {
        log.info("AuhtController#login called with username: " + user.getUsername());
        String result;
        try {
            result = authService.login(user);
        } catch (Exception e) {
            log.info(e.getMessage());
            result = null;
        }

       if (result != null && !result.isEmpty()) {
           HttpHeaders authHeader = new HttpHeaders();
           //authHeader.add("WWW-Authenticate", "Bearer " + result);
           authHeader.add("Location", "/");
           Cookie tokenCookie = new Cookie("token", result);
           tokenCookie.setHttpOnly(true);
           tokenCookie.setSecure(true);
           tokenCookie.setPath("/");
           tokenCookie.setAttribute("SameSite", "Strict");
           response.addCookie(tokenCookie);
          return new ResponseEntity<>(authHeader, HttpStatus.FOUND);
       }
       else {
           HttpHeaders redirect = new HttpHeaders();
           redirect.add("Location", "/auth/login?error=true");
           return new ResponseEntity<>(redirect, HttpStatus.FOUND);
       }
    }

    @PostMapping(
            path = "/auth/web/signup",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public ResponseEntity<String> handleWebSignup(UserDto user, HttpServletResponse response) {
        log.info("AuhtController#signup called with username: " + user.getUsername() + " password: " + user.getPassword());
        String result = authService.signup(user);
        HttpHeaders headers = new HttpHeaders();
        if(result == null) {
            headers.add("Location", "/auth/sigunp?error=true");
            return new ResponseEntity<>(headers, HttpStatus.FOUND);
        }
        headers.add("location", "/");
        Cookie tokenCookie = new Cookie("token", result);
        tokenCookie.setHttpOnly(true);
        tokenCookie.setSecure(true);
        tokenCookie.setPath("/");
        tokenCookie.setAttribute("SameSite", "Strict");
        response.addCookie(tokenCookie);

        return new ResponseEntity<>(headers, HttpStatus.FOUND);
    }

    @PostMapping("/addRole/{username}/{role}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> addRoleToUser(@PathVariable String username, @PathVariable String role) {
        authService.addRoleToUser(username, role);

        return ResponseEntity.ok().build();
    }

}
