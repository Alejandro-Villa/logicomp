package com.alejandrovp.logicomp.controller;

import com.alejandrovp.logicomp.dto.OrderDto;
import com.alejandrovp.logicomp.dto.models.OrderModelAssembler;
import com.alejandrovp.logicomp.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(path = "/api/order")
@RequiredArgsConstructor
@Secured("ROLE_MANAGER")
public class OrderController {
    private final OrderService orderService;
    private final OrderModelAssembler orderModelAssembler;

    @GetMapping("/order/{id}")
    public @ResponseBody EntityModel<OrderDto> one(@PathVariable long id) {
        return orderModelAssembler.toModel(
                orderService.getOrderById((int) id).orElseThrow()
        );
    }

    @GetMapping("/all")
    public CollectionModel<EntityModel<OrderDto>> all() {
        List<EntityModel<OrderDto>> found = orderService.getAll().stream()
                .map(orderModelAssembler::toModel)
                .toList();
        return CollectionModel.of(found, linkTo(methodOn(OrderController.class).all()).withSelfRel());
    }

    @PostMapping("/new")
    public ResponseEntity<?> create(@RequestBody OrderDto newOrder) {
        EntityModel<OrderDto> model = orderModelAssembler.toModel(
                orderService.createOrder(newOrder)
        );

        return ResponseEntity
                .created(model.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(model);
    }

    @PutMapping("/order/{id}")
    public ResponseEntity<?> update(@RequestBody OrderDto newOrder, @PathVariable long id) {
        EntityModel<?> updated = orderModelAssembler.toModel(
                orderService.update(newOrder, id));
        return ResponseEntity
                .created(updated.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(updated);
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/cargo/{id}")
    public ResponseEntity<?> delete(@PathVariable long id) {
        orderService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
