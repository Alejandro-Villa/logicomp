package com.alejandrovp.logicomp.util;

import jakarta.persistence.Embeddable;
import lombok.extern.java.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Log
@Embeddable
public class PlateNumber {
    private String plateNumber;

    public PlateNumber(String plateNumber) {
        String regex = "^([0-9]{4})([a-zA-z]{3})$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(plateNumber);

        if(matcher.find()) {
            log.info("Storing " + matcher.group(1) + matcher.group(2));
            int number = Integer.parseInt(matcher.group(1));
            String letters = matcher.group(2);
            this.plateNumber = String.format("%d%s", number, letters);
        }
        else {
            log.severe("Matcher failed with input " + plateNumber + " using regex " + matcher.pattern().pattern());
            throw new RuntimeException();
        }
    }

    @Override
    public String toString() {
        return getPlateNumber();
    }

    private boolean checkString(String input) {
        String regex = "^([0-9]{4})([a-zA-z]{3})$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(plateNumber);

        return input.length() == 7 && matcher.find();
    }

    public PlateNumber() {
        plateNumber = "";
    }

    public boolean setPlateNumber(String input) {
        if(checkString(input)) {
            plateNumber = input;
            return true;
        }
        return false;
    }

    public String getPlateNumber() {
        return plateNumber;
    }
}
