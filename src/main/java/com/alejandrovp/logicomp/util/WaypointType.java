package com.alejandrovp.logicomp.util;

public enum WaypointType {
    LOAD,
    UNLOAD
}
