package com.alejandrovp.logicomp.util;

public enum DriverStatus {
    DRIVING,
    CO_DRIVING,
    MOVING_CARGO,
    RESTING
}
