package com.alejandrovp.logicomp.util;

public enum CargoStatus {
    READY,
    SHIPPED,
    DELIVERED,
}
