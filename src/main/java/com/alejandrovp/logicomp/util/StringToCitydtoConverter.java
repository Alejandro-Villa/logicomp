package com.alejandrovp.logicomp.util;

import com.alejandrovp.logicomp.dto.CityDto;
import com.alejandrovp.logicomp.service.CityService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

@Data
public class StringToCitydtoConverter implements Converter<String, CityDto> {
    @Autowired
    private CityService cityService;

    @Override
    public CityDto convert(String source) {
        if(source.isEmpty())
            return null;

        return cityService.getCityByName(source).orElseThrow();
    }
}
