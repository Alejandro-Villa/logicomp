package com.alejandrovp.logicomp.mapper;

import com.alejandrovp.logicomp.dto.CityDto;
import com.alejandrovp.logicomp.entity.CityEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CityEntityDtoMapper {
    CityEntity DtoToEntity(CityDto cityDto);
    CityDto EntityToDto(CityEntity cityEntity);
}
