package com.alejandrovp.logicomp.mapper;

import com.alejandrovp.logicomp.dto.DriverDto;
import com.alejandrovp.logicomp.entity.DriverEntity;
import org.mapstruct.Context;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DriverEntityDtoMapper {
    DriverEntity DtoToEntity(DriverDto driverDto, @Context CycleAvoidingMappingContext cycleAvoidingMappingContext);
    DriverDto EntityToDto(DriverEntity driverEntity, @Context CycleAvoidingMappingContext cycleAvoidingMappingContext);

    @DoIgnore
    default DriverEntity DtoToEntity(DriverDto driverDto) {
        return DtoToEntity(driverDto, new CycleAvoidingMappingContext());
    }

    @DoIgnore
    default DriverDto EntityToDto(DriverEntity driverEntity) {
        return EntityToDto(driverEntity, new CycleAvoidingMappingContext() );
    }
}
