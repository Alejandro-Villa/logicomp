package com.alejandrovp.logicomp.mapper;

import com.alejandrovp.logicomp.dto.CargoDto;
import com.alejandrovp.logicomp.entity.CargoEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring" )
public interface CargoEntityDtoMapper {
   CargoEntity DtoToEntity(CargoDto cargoDto);
   CargoDto EntityToDto(CargoEntity cargoEntity);
}
