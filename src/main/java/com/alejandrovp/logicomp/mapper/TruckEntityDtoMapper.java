package com.alejandrovp.logicomp.mapper;

import com.alejandrovp.logicomp.dto.TruckDto;
import com.alejandrovp.logicomp.entity.TruckEntity;
import org.mapstruct.Context;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TruckEntityDtoMapper {
    TruckEntity DtoToEntity(TruckDto truckDto, @Context CycleAvoidingMappingContext cycleAvoidingMappingContext);
    TruckDto EntityToDto(TruckEntity truckEntity, @Context CycleAvoidingMappingContext cycleAvoidingMappingContext);

    @DoIgnore
    default TruckEntity DtoToEntity(TruckDto truckDto) {
        return DtoToEntity(truckDto, new CycleAvoidingMappingContext());
    }

    @DoIgnore
    default TruckDto EntityToDto(TruckEntity truckEntity) {
        return EntityToDto(truckEntity, new CycleAvoidingMappingContext());
    }
}
