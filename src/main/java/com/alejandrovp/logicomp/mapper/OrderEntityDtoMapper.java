package com.alejandrovp.logicomp.mapper;

import com.alejandrovp.logicomp.dto.OrderDto;
import com.alejandrovp.logicomp.entity.OrderEntity;
import org.mapstruct.Context;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderEntityDtoMapper {
    OrderEntity DtoToEntity(OrderDto orderDto, @Context CycleAvoidingMappingContext cycleAvoidingMappingContext);
    OrderDto EntityToDto(OrderEntity orderEntity, @Context CycleAvoidingMappingContext cycleAvoidingMappingContext);

    @DoIgnore
    default OrderEntity DtoToEntity(OrderDto orderDto) {
        return DtoToEntity(orderDto, new CycleAvoidingMappingContext());
    }

    @DoIgnore
    default OrderDto EntityToDto(OrderEntity orderEntity) {
        return EntityToDto(orderEntity, new CycleAvoidingMappingContext());
    }
}
