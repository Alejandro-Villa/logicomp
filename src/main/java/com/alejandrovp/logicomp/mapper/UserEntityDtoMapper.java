package com.alejandrovp.logicomp.mapper;

import com.alejandrovp.logicomp.dto.UserDto;
import com.alejandrovp.logicomp.entity.UserEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserEntityDtoMapper {
    UserEntity DtoToEntity(UserDto userDto);
    UserDto EntityToDto(UserEntity userEntity);
}
