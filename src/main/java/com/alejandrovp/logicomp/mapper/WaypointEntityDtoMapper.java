package com.alejandrovp.logicomp.mapper;

import com.alejandrovp.logicomp.dto.WaypointDto;
import com.alejandrovp.logicomp.entity.WaypointEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface WaypointEntityDtoMapper {
    WaypointEntity DtoToEntity(WaypointDto waypointDto);
    WaypointDto EntityToDto(WaypointEntity waypointEntity);
}
